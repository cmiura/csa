import sys
import pickle
import pandas as pd
from sklearn import model_selection


from cascade import *
from  binary_evaluation import *






data = pd.read_csv("../io/data2.csv", sep=",", encoding="utf-8")
data=data[["clip.csa_id","clip.description","program.title"]]
data.columns=["label","description","program_title"]


data = data.dropna(subset=["description"])
data = data.drop_duplicates(subset=["description"])
data['description'] = data['description'].map(lambda x:x.replace("040",". ").lower())
columns = ['label', 'text', 'program_title']
data.columns = columns
print(len(data))





data2=data.copy()
data2.label[data2.label.isin([2,3])]=2
data2.label[data2.label.isin([4,5])]=3


#fonction qui trouve le threshold de classification optimal pour la classification binaire ou la classe la moins sensible est la classe i
def optimal_threshold(i, vectorizer, classifier, metric, data):
    subset=data[data.label>=i]
    X=subset["text"]
    y=subset.label.apply(lambda x: 0 if x==i else 1)
    X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y, test_size=0.60)
    #comme les performances du modèle sont très stables à partir de 30000 obs (ie 40% de la base totale), on se permet de prendre une base test plus grosse
    #ce sera mieux pour trouver les thresholds optimaux
    cv = vectorizer
    results = cv.fit_transform(X_train)
    results_valid = cv.transform(X_valid)
    model = classifier
    model=model.train(results, y_train)
    y_probas=model.predict_proba(results_valid)
    binary_eval = Evaluation(y_valid, y_probas)
    binary_eval.find_optimal_threshold(metric=metric)
    return(binary_eval.optimal_threshold)

vectorizer=CountVectorizer()
classifier=LogisticRegressionClassifier()
thresholds={1:optimal_threshold(1, vectorizer, classifier, "precision_recall",data2), 2:optimal_threshold(2, vectorizer, classifier, "precision_recall", data2)}
print(thresholds)
pickle.dump(thresholds, open('my_thresholds.pkl', 'wb'))
