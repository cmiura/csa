import sys
import pickle
import pandas as pd
from sklearn import model_selection


from cascade import *
from  binary_evaluation import *




data = pd.read_csv("../io/data2.csv", sep=",", encoding="utf-8")
data=data[["clip.csa_id","clip.description","program.title"]]
data.columns=["label","description","program_title"]
data = data.dropna(subset=["description"])
data = data.drop_duplicates(subset=["description"])
data['description'] = data['description'].map(lambda x:x.replace("040",". ").lower())
columns = ['label', 'text', 'program_title']
data.columns = columns



#on regroupe 2-3 ensemble et 4-5 ensemble
data2=data.copy()
data2.label[data2.label.isin([2,3])]=2
data2.label[data2.label.isin([4,5])]=3

X=data2["text"]
y=data2.label
X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y)


#Quels thresholds choisir pour les 2 classi binaires successives? 
#On choisit ici de maximiser à chaque fois la précision-recall
#On récupère le pickle créé grâce au fichier find_the_best_classification_thresholds.py



thresholds=pickle.load(open('my_thresholds.pkl', 'rb'))
print(thresholds)
vectorizer=CountVectorizer()
classifier=LogisticRegressionClassifier()
cascade_classifier=CascadeClassifier(vectorizer, classifier, thresholds)
cascade_classifier=cascade_classifier.train(X_train,y_train)

#si on veut récupérer le modèle en entier
pickle.dump(cascade_classifier, open('my_cascade_classifier.pkl', 'wb'))

#si on veut récupérer ses paramètres

#récupération du vocabulaire du count_vectorizer
cv=cascade_classifier.vectorizer
voc=cv.sparse_vectorizer.vocabulary_ #vocabulaire =word_to_id

#récupération des paramètres des deux régressions logistiques en cascade
models=cascade_classifier.cascade_model #liste des deux modèles entrainés des régressions logistiques
hyperparameters=models[0].model.get_params() #les hyperparamètres du modèle (les mêmes pour les deux régressions)
parameters1=models[0].model.coef_[0] #les coeffs de la première régression logistique
intercept1=models[0].model.intercept_[0] #on doit aussi récupérer le biais!!!
parameters1=np.append(parameters1, intercept1) #ajout du biais à la fin de l'array
parameters2=models[1].model.coef_[0] #les coeffs de la deuxième régression logistique
intercept2=models[1].model.intercept_[0]
parameters2=np.append(parameters2, intercept2)


#dictionnaire comprenant les principales caractéristiques du modèle
#on a le vocabulaire
#la requête sql
#les paramètres des deux régressions logistiques (la 1ère Tous publics VS sensibles et la deuxième : -10/-12 VS 1-16/-18)
#les hyperparamètres sont les mêmes pour les deux régressions

d={"model_id":"logistic_regression_28/01/2018",
"data":"""  select 
    clip.id,         
    clip.csa_id, 
    clip.description, 
    clip.metadescription , 
    csa.age,
    clip.program_id,
    program.title,
    collect_set(clip_tag.value)  as TagValue 
from sqoop_backstage_video.clip_tag 
    inner join sqoop_backstage_video.clip
        on clip.id = clip_tag.clip_id 
    inner join sqoop_backstage_video.csa
        on csa.id == clip.csa_id
    inner join sqoop_backstage_video.program
        on program.id==clip.program_id
        
group by clip.id, 
        clip.csa_id, 
        clip.description, 
        clip.metadescription,
        csa.age,
        clip.program_id,
        program.title   """ ,
   
"parameters1":parameters1 ,
"parameters2":parameters2,
"hyperparameters":hyperparameters,
"word_to_id":voc,
"thresholds":thresholds}  

pickle.dump(d, open('my_model_parameters.pkl', 'wb'))