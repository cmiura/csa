import numpy as np
import pandas as pd
import json
import re
import string

# tokenizer
from stopwords import stopwords
from gensim.utils import simple_preprocess
from gensim.models.phrases import Phrases, Phraser


def preprocess(text):
    ## Remove puncuation
    text = text.translate(string.punctuation)
    ## Convert words to lower case and split them
    text = text.lower().split()
    stops = set(stopwords)
    text = [w for w in text if not w in stops and len(w) >= 3]
    text = " ".join(text)
    # Clean the text
    text = re.sub(r"[^A-ZÆÇÈÉÊËÎÏÔÛa-zàâéèëêïîôùûç0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
    ## Remove stop words
    text = text.split()
    stops = set(stopwords)
    text = [w for w in text if not w in stops and len(w) >= 3]
    text = " ".join(text)
    
    return text

def merge_proper_names(text):
    """Merge the proper names by replacing non-alphabetic characters by underscores
    We concatenate suites of words (separated by non-alpha characters) 
    with a first letter capitalized
    
    Args:
        text (str): text where we want to merge proper names
    Returns: 
        text with names merged
    """
    # premier traitement pour éviter de rater les noms propres séparés par
    # `d'`, `et` ou `le` comme dans "Shirley et Dino", ou "Côte d'Ivoire" 
    text = re.sub(r"( [a-z]{,2} )|( [a-z]’)", " ", text)
    fr_lower = "a-zàâéèëêïîôùûç"# æ"
    fr_upper = "A-ZÆÇÈÉÊËÎÏÔÛ"
    fr = fr_lower + fr_upper
    proper_names_pattern = (r'[^.][^.][^{fr}]([{fu}][{fr}]+' + 
                            r'([^.,;:{fr}][{fu}][{fr}]+)+)').format(fr=fr, fu=fr_upper)
    proper_names = re.findall(proper_names_pattern, text)
    for pn, _ in proper_names:
        new_pn = re.sub('[^%s]' % fr, '_', pn)
        text = text.replace(pn, new_pn)
    return text


class Tokenizer():
    """Class used to tokenize text"""
    
    PHRASES_KWS_DEFAULT = {"min_count": 30, "threshold": 4000}
    
    def __init__(self, merge_proper=False, phrases=False, min_len=3, 
                 phrases_kws=PHRASES_KWS_DEFAULT):
        """Creates the Tokenizer instance that will be used to tokenize text
        
        Args:
            merge_proper (bool): defines if we merge proper names or not before tokenizing
                                 (criteria on capitalization)
            phrases (bool): defines if we merge common phrases (criteria on frequency)
            min_len (int):  words with less than min_len letters are discarded
            phrases_kws (dict): dictionary with keywords used to build 
                                the gensim.models.phrases.Phrases model
        Returns: 
            the Tokenizer instance
        """
        self.merge_proper = merge_proper
        self.min_len = min_len
        self.phrases = phrases
        self.phrases_kws = phrases_kws
        self.phraser = None
        
        
    def _tokenize_sentence(self, s):
        """Tokenizes a single sentence (or string)
        We may merge proper names
        We tokenize, remove stopwords and strip the final `s` of words
        
        Args:
            s (str): string of text we want to tokenize
        Returns:
            list of words from tokenized sentence
        """
        if self.merge_proper:
            s = merge_proper_names(s) # little stopwords
        tokens = simple_preprocess(s, min_len=self.min_len) # premier tokenizing
        return [t.rstrip("s") for t in tokens if t not in stopwords]
    
    
    def tokenize(self, sentences):
        """Tokenizes a group of sentences or a single sentence
        
        Args:
            sentences (str or pd.Series): single string sentence or group of sentences
        Returns:
            sentence or group of sentences depending of the argument type, tokenized"""
        if isinstance(sentences, pd.Series):
            sentences = sentences.apply(self._tokenize_sentence)
            if self.phrases:
                phrases_model = Phrases(sentences, **self.phrases_kws)
                phraser = Phraser(phrases_model)
                self.phraser = phraser
                return pd.Series(list(self.phraser[sentences]), index=sentences.index)
            else:
                return sentences
        else:
            sentences = self._tokenize_sentence(sentences)
            if self.phrases and self.phraser is not None:
                return pd.Series(list(self.phraser[sentences]))
            else:
                return sentences