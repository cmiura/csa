import numpy as np
from sklearn import metrics
#import scikitplot as skplt
#import matplotlib.pyplot as plt

class Evaluation:
    def __init__(self, labels, y_proba):
        self.y_true = np.array(labels)
        self.y_proba = y_proba
        self.optimal_threshold = 0.5
        
    def find_optimal_threshold(self, metric='youdens'):
        assert len(np.unique(self.y_true)) == 2, "That's not binary labels"    
        if metric == 'precision_recall':
            precision, recall, threshold = metrics.precision_recall_curve(self.y_true, self.y_proba)
            f1_scores = 2*(precision * recall)/(precision + recall)
            optimal_idx = np.argmax(f1_scores[:-1]) #on enlève la dernière valeur qui correspond à precision de 1 et recall de 0
            self.optimal_threshold = threshold[optimal_idx]
        elif metric == 'youdens':
            fpr, tpr, threshold = metrics.roc_curve(self.y_true, self.y_proba)
            youdens_scores = tpr - fpr
            optimal_idx = np.argmax(youdens_scores)
            self.optimal_threshold = threshold[optimal_idx]
        elif metric == 'recall':
            precision, recall, threshold = metrics.precision_recall_curve(self.y_true, self.y_proba)
            optimal_idx = np.argmax(recall[:-1]) #la dernière valeur de recall est tjrs 0 et n'a pas de threshold correspondant donc on l'enlève
            self.optimal_threshold = threshold[optimal_idx]
        elif metric == 'precision':
            precision, recall, threshold = metrics.precision_recall_curve(self.y_true, self.y_proba)
            optimal_idx = np.argmax(precision[:-1]) #la dernière valeur de precision est tjrs 1 et n'a pas de threshold correspondant donc on l'enlève
            self.optimal_threshold = threshold[optimal_idx]
        return self.optimal_threshold

    
    def f1_score(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        return metrics.f1_score(self.y_true, self.y_pred)

    def recall_score(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        return metrics.recall_score(self.y_true, self.y_pred)

    def precision_score(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        return metrics.precision_score(self.y_true, self.y_pred)


        
    def accuracy(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        return metrics.accuracy_score(self.y_true, self.y_pred)
    
    def confusion_matrix(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        return metrics.confusion_matrix(self.y_pred, self.y_true)
    
    def classification_report(self):
        self.y_pred = np.array([1 if self.y_proba[i] >=self.optimal_threshold else 0 for i in range(len(self.y_proba))])
        print(metrics.classification_report(self.y_true, self.y_pred))
    
    def roc_auc_score(self):
        return metrics.roc_auc_score(self.y_true, self.y_proba)
    
    def log_loss(self):
        return metrics.log_loss(self.y_true, self.y_proba)
    

    def plot_roc_curve(self):
        all_proba = np.zeros((len(self.y_proba), 2))
        all_proba[:,0] = 1 - self.y_proba
        all_proba[:,1] = self.y_proba
        skplt.metrics.plot_roc(self.y_true, all_proba, figsize=(10,5), 
                               plot_micro=False, plot_macro=False, classes_to_plot=[0,1])
        plt.show()

        
    def plot_precision_recall_curve(self):
        all_proba = np.zeros((len(self.y_proba), 2))
        all_proba[:,0] = 1 - self.y_proba
        all_proba[:,1] = self.y_proba
        skplt.metrics.plot_precision_recall(self.y_true, all_proba, figsize=(10,5), 
                               plot_micro=False, classes_to_plot=[0,1])
        plt.show()

    

