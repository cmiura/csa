import numpy as np
import sklearn.feature_extraction
import sklearn.decomposition
#from collections import defaultdict
from preprocessing_text import Tokenizer, preprocess, stopwords
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import keras.preprocessing.text as keras_tokenizer
import keras.preprocessing.sequence as keras_padding



class Vectorizer:   
    def __init__(self):
        pass
        
    def fit_transform(X):
        raise NotImplementedError('Abstract Class')
        
    def transform(X):
        raise NotImplementedError('Abstract Class')
        
class SparseVectorizer(Vectorizer):
    def __init__(self, analyzer='word', token_pattern=r'\w{1,}', stop_words=stopwords):
        self.sparse_vectorizer = None
        self.analyzer = analyzer
        self.token_pattern = token_pattern
        self.stop_words = stop_words
        
    def transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        return self.sparse_vectorizer.transform(X)
    
class CountVectorizer(SparseVectorizer):
    def fit_transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        self.sparse_vectorizer = sklearn.feature_extraction.text.CountVectorizer(analyzer=self.analyzer, token_pattern=self.token_pattern,
                                                                     stop_words=self.stop_words)
        return self.sparse_vectorizer.fit_transform(raw_documents=X)
    
class TfidfVectorizer(SparseVectorizer):
    def fit_transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        self.sparse_vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(analyzer=self.analyzer, token_pattern=self.token_pattern,
                                                                     stop_words=self.stop_words)
        return self.sparse_vectorizer.fit_transform(raw_documents=X)

class HashingVectorizer(SparseVectorizer):
    def fit_transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        self.sparse_vectorizer = sklearn.feature_extraction.text.HashingVectorizer(analyzer=self.analyzer, token_pattern=self.token_pattern,
                                                                     stop_words=self.stop_words)
        return self.sparse_vectorizer.fit_transform(X=X)

class DenseVectorizer(Vectorizer):
    pass

class Doc2VecVectorizer(DenseVectorizer):
    def __init__(self, vector_size=100, window=2, min_count=1, workers=4, epochs=10):
        self.model = None
        self.vector_size = vector_size
        self.window = window
        self.min_count = min_count
        self.workers = workers
        self.epochs = epochs
        
    def fit_transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        tokenizer = Tokenizer()
        tokens = tokenizer.tokenize(X)
        documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(tokens)]
        self.model = Doc2Vec(documents, vector_size=self.vector_size, window=self.window, 
                             min_count=self.min_count, workers=self.workers, epochs=self.epochs)
        results = np.zeros((len(documents), self.vector_size))
        for i in range(len(documents)) : results[i] = self.model.docvecs[i]
        return results
    
    def transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        tokenizer = Tokenizer()
        tokens = tokenizer.tokenize(X)
        results = np.zeros((len(tokens), self.vector_size))
        for i in range(len(tokens)) : results[i] = self.model.infer_vector(tokens.iloc[i])
        return results

class SVDVectorizer(DenseVectorizer):
    def __init__(self, n_components=300, n_iter=20, algorithm='randomized', random_state=None, tol=0.0):
        self.svd_vectorizer = None
        self.n_components = n_components
        self.n_iter = n_iter
        self.algorithm = algorithm
        self.random_state = random_state
        self.tol = tol
        
    def fit_transform(self, X_sparse):
        self.svd_vectorizer = sklearn.decomposition.TruncatedSVD(n_components=self.n_components, n_iter=self.n_iter,
                                                      algorithm=self.algorithm, random_state=self.random_state, tol=self.tol)        
        return self.svd_vectorizer.fit_transform(X_sparse)
    
    def transform(self, X):
        return self.svd_vectorizer.transform(X)

class Word2VecVectorizer(DenseVectorizer):
    def __init__(self, pre_trained_vectors_path=None):
        self.embeddings_index = None
        self.tokenizer = None
        self.len_max = None
        if pre_trained_vectors_path is not None:            
            embeddings_index = dict()
            f = open(pre_trained_vectors_path, "rb")
            for line in f:
                values = line.split()
                word = values[0].decode('utf-8')
                coefs = np.asarray(values[1:], dtype='float32')
                embeddings_index[word] = coefs
            f.close()
            print('Loaded %s word vectors.' % len(embeddings_index))
            self.embeddings_index = embeddings_index
            self.dim = len(list(embeddings_index.values())[0])
        
    def fit_transform(self, X):
        new_X = X.apply(lambda x: preprocess(x))
        self.tokenizer = keras_tokenizer.Tokenizer()
        self.tokenizer.fit_on_texts(new_X)        
        sequences = self.tokenizer.texts_to_sequences(new_X)
        self.len_max = max([len(i) for i in sequences])
        new_data = keras_padding.pad_sequences(sequences, maxlen=self.len_max, padding='post')
        if self.embeddings_index is not None:          
            # create a weight matrix for words in training docs# create
            word_index = self.tokenizer.word_index.items()
            vocabulary_size = len(word_index)
            embedding_matrix = np.zeros((vocabulary_size+1, self.dim))
            for word, index in word_index:
                if index > vocabulary_size - 1:
                    break
                else:
                    embedding_vector = self.embeddings_index.get(word)
                    if embedding_vector is not None:
                        embedding_matrix[index] = embedding_vector
            return new_data, embedding_matrix
        else:
            return new_data
        
    def transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        sequences = self.tokenizer.texts_to_sequences(X)
        return keras_padding.pad_sequences(sequences, maxlen=self.len_max, padding='post')
    

class MeanEmbeddingVectorizer(Word2VecVectorizer):
    def fit_transform(self, X):
        assert self.embeddings_index is not None
        X = X.apply(lambda x: preprocess(x))
        tokenizer = Tokenizer()
        tokens = tokenizer.tokenize(X)
        # if a text is empty we should return a vector of zeros
        # with the same dimensionality as all the other vectors
        return np.array([
            np.mean([self.embeddings_index[w] for w in words if w in self.embeddings_index]
                    or [np.zeros(self.dim)], axis=0)
            for words in tokens
        ])
    
    def transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        return self.fit_transform(X)
 
"""   
class TfidfEmbeddingVectorizer(Word2VecVectorizer):
    def fit_transform(self, X):
        assert self.embeddings_index is not None
        X = X.apply(lambda x: preprocess(x))
        tfidf = sklearn.feature_extraction.text.TfidfVectorizer(analyzer=lambda x: x)
        tfidf.fit(X)
        # if a word was never seen - it must be at least as infrequent
        # as any of the known words - so the default idf is the max of 
        # known idf's
        max_idf = max(tfidf.idf_)
        word2weight = defaultdict(
            lambda: max_idf,
            [(w, tfidf.idf_[i]) for w, i in tfidf.vocabulary_.items()])
        
        return np.array([
                np.mean([self.embeddings_index[w] * word2weight[w]
                         for w in words if w in self.embeddings_index] or
                        [np.zeros(self.dim)], axis=0)
                for words in X
            ])
    
    def transform(self, X):
        X = X.apply(lambda x: preprocess(x))
        return self.fit_transform(X)"""


if __name__ == '__main__':
    print("coucou")