import numpy as np
from sklearn import linear_model, ensemble, metrics
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Flatten, LSTM, Conv1D, MaxPooling1D, Dropout, Activation
from keras.layers.embeddings import Embedding


class Classifier:   
    def __init__(self):
        self.model = None
    
    def train(X, labels): raise NotImplementedError('Abstract Class')    
    def predict(self, X_valid): raise NotImplementedError('Abstract Class')    
    def predict_proba(self, X_valid): raise NotImplementedError('Abstract Class')
     



class MachineClassifier(Classifier):
    def train(self, X, labels):
        self.model.fit(X, np.array(labels))
        return self
    
    def predict_proba(self, X_valid):
        y_proba = self.model.predict_proba(X_valid)
        if y_proba.shape[1] == 2:
            y_proba_positive = y_proba[:,1]
            return y_proba_positive
        else:
            return self.model.predict_proba(X_valid)
    
    def predict(self, X_valid, threshold=0.5):
        y_proba = self.model.predict_proba(X_valid)
        if y_proba.shape[1] == 2:
            y_proba_positive = y_proba[:,1]
            y_pred = np.array([1 if y_proba_positive[i] >=threshold else 0 for i in range(len(y_proba_positive))])
            return y_pred
        else:
            return self.model.predict(X_valid)
    
class LogisticRegressionClassifier(MachineClassifier):


    def __init__(self, **kwargs):
        self.model = linear_model.LogisticRegression(**kwargs)        

        
class RandomForestClassifier(MachineClassifier):
    def __init__(self, **kwargs):
        self.model = ensemble.RandomForestClassifier(**kwargs)
        
    def features_importances(self, n, feature_names=None):
        importances = self.model.feature_importances_
        indices = np.argsort(importances)[::-1]
        print("Feature ranking:")
        for f in range(n):
            if feature_names is None:
                print("%d. feature %d %s (%f)" % (f + 1, indices[f], importances[indices[f]]))
            else:
                print("%d. feature %d %s (%f)" % (f + 1, indices[f], feature_names[indices[f]], importances[indices[f]]))

class GradientBoostingClassifier(MachineClassifier):
    def __init__(self, **kwargs): 
        self.model = ensemble.GradientBoostingClassifier(**kwargs)
        
class DeepClassifier(Classifier):
    def train(self, X, labels, validation_split=0.4, epochs=1):
        self.model.fit(X, np.array(labels), validation_split=validation_split, epochs=epochs)
        return self
    
    def predict_proba(self, X_valid):
        y_proba = self.model.predict_proba(X_valid)
        if y_proba.shape[1] == 1:
            y_proba_positive = y_proba.flatten()
            return y_proba_positive
        else:
            return y_proba
        
    def predict(self, X_valid, threshold=0.5):
        y_proba = self.model.predict_proba(X_valid)
        if y_proba.shape[1] == 1:
            y_proba_positive = y_proba.flatten()
            y_pred = np.array([1 if y_proba_positive[i] >=threshold else 0 for i in range(len(y_proba_positive))])
            return y_pred
        else:
            return self.model.predict(X_valid)
    
class MLPClassifier(DeepClassifier):
    def __init__(self, input_dim, input_length, output_dim=100, embedding_matrix=None, dropout=0.2, recurrent_dropout=0.2, 
                activation='sigmoid', loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'],
                nbr_labels=1):
        
        trainable = True
        weights = None
        if embedding_matrix is not None: trainable = False
        if embedding_matrix is not None: weights = [embedding_matrix]        
        self.model = Sequential()
        self.model.add(Embedding(input_dim=input_dim, output_dim=output_dim, input_length=input_length,
                                      weights=weights, trainable=trainable))
        self.model.add(Flatten())
        self.model.add(Dense(units=nbr_labels, activation=activation))
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        

class LSTMClassifier(DeepClassifier):
    def __init__(self, input_dim, input_length, output_dim=100, embedding_matrix=None, dropout=0.2, recurrent_dropout=0.2, 
                activation='sigmoid', loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'],
                nbr_labels=1):
        
        trainable = True
        weights = None
        if embedding_matrix is not None: trainable = False
        if embedding_matrix is not None: weights = [embedding_matrix]        
        self.model = Sequential()
        self.model.add(Embedding(input_dim=input_dim, output_dim=output_dim, input_length=input_length,
                                      weights=weights, trainable=trainable))
        self.model.add(LSTM(units=100, dropout=dropout, recurrent_dropout=recurrent_dropout))
        self.model.add(Dense(units=nbr_labels, activation=activation))
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)

    
class CNNClassifier(DeepClassifier):
    def __init__(self, input_dim, input_length, output_dim=100, embedding_matrix=None, dropout=0.2, activation_conv='relu', 
                activation_dense='sigmoid', loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'],
                nbr_labels=1):
        
        trainable = True
        weights = None
        if embedding_matrix is not None: trainable = False
        if embedding_matrix is not None: weights = [embedding_matrix]         
        self.model = Sequential()
        self.model.add(Embedding(input_dim=input_dim, output_dim=output_dim, input_length=input_length,
                                      weights=weights, trainable=trainable))
        self.model.add(Dropout(rate=dropout))
        self.model.add(Conv1D(filters=64, kernel_size=5, activation=activation_conv))
        self.model.add(MaxPooling1D(pool_size=4))
        self.model.add(LSTM(units=100))
        self.model.add(Dense(units=nbr_labels, activation=activation_dense))
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        

        


