from vectorizer import *
from classifier import *
import copy
import pandas as pd


class CascadeClassifier():
    def __init__(self, vectorizer, classifier, thresholds):
        self.cascade_model = []
        self.vectorizer = vectorizer
        self.classifier = classifier
        self.thresholds=thresholds
    
    def build_subsets(self, X, labels):
        classes = np.unique(labels)
        x_change = self.vectorizer.fit_transform(X)
        y_change = labels.copy()
        y_change = y_change.apply(lambda x: 0 if x==classes[0] else 1)
        subsets = [(x_change, y_change)]
        for i in range(1, len(classes)-1):
            x_change = self.vectorizer.transform(X[labels.isin(classes[i:])])
            y_change = labels[labels.isin(classes[i:])]
            y_change = y_change.apply(lambda x: 0 if x==classes[i] else 1)
            subsets.append((x_change, y_change))
        return subsets
    
    def train(self, X, labels):
        training_subsets = self.build_subsets(X, labels)
        for subset in training_subsets:
            classifier = copy.deepcopy(self.classifier)
            classifier.train(subset[0], subset[1])
            self.cascade_model.append(classifier)
        return self
    
    def predict(self, X_valid):   #thresholds est un dictionnaire des thresholds optimal pour les classi binaires successives
        
        y_pred = pd.Series(len(self.cascade_model)+1, index=X_valid.index)
        for i, model_part in enumerate(self.cascade_model):
            td=self.thresholds[i+1]
            y_predict = model_part.predict(self.vectorizer.transform(X_valid), threshold=td)
            X_1 = X_valid[y_predict==0]
            y_pred[X_1.index] = i+1
            if sum(y_predict==0)!=0:
                X_valid = X_valid[y_predict==1]
            else:
                break            
        return y_pred
    
    
    def predict_un_texte(self, mon_texte):
        mon_texte=pd.Series(mon_texte)
        i=0
        while i < len(self.cascade_model):
            td=self.thresholds[i+1]
            y_predict = self.cascade_model[i].predict(self.vectorizer.transform(mon_texte), threshold=td)[0]
            if y_predict==0:
                return(i+1)
            else:
                i=i+1
        return(i+1)