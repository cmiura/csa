from flask import *
import requests, json
import sys, os

def get_texte_prediction(texte):
    print("* Excecuting query")
    #url="http://localhost:5000/csa" #adresse du serveur qui sert la classification binaire (ici on est en local)
    url="http://localhost:4000/csa" #adresse du serveur qui sert la classification multiclasse
    #url="http://172.31.43.214:4000/csa"
    
    
    print("* Request url " + url)
    input_data=json.dumps({"texte": texte})


    # On envoie le message au serveur grace a la methode post
    out=requests.post(url,input_data)
    
    print("*********RESULTATS DE LA PREDICTION***********")
    #On recupere le resultat du serveur dans un objet result
    
    result = out.json()
    print(result)






if __name__ == "__main__":
    print("Hello")
    while True:
        lettre = input("Tapez 'q' pour quitter ou tapez 'c' pour continuer: ")
        if lettre == "q":
                print("Fermeture du programme")
                sys.exit()
        
        else:
            texte_choosen = input(" \n\n Entrez votre texte  svp: ")

            get_texte_prediction(texte_choosen)

            

        

        
