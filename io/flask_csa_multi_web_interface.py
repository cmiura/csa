from flask import Flask, request, render_template, jsonify
import pickle




cascade_classifier = pickle.load(open('my_cascade_classifier', 'rb'))


"""Ici la base d'entrainement est la base de M6"""

app = Flask(__name__)

@app.route('/')
def accueil():
    return """<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <title>{titre}</title>
            </head>
        
            <body>
                <h1>{titre}</h1>
   
                <a href="/csa">Cliquez ici pour prédire la sensibilité de votre texte</a>
                
            </body>
        </html>""".format(titre="Bienvenue !")




@app.route('/csa', methods=['GET', 'POST'])
def prediction():
    if request.method == 'POST':
        texte=request.form["description"]
        pred=cascade_classifier.predict_un_texte(texte)
        d={1:"le_texte a un contenu Tous publics", 2:"Le texte a un contenu interdit aux moins de 10 ans ou 12 ans", 3:"Le texte a un contenu interdit aux moins de 16 ou 18 ans"}
        return jsonify(label=pred, texte=d[pred])
    return 'Entrez votre texte entre guillemets svp <br> <form action="" method="post"><input type="text" name="description" /><input type="submit" value="Envoyer" /></form>'



if __name__ == '__main__':
    app.run(debug=True)




