import pickle
from flask import *
import sys 
import numpy as np

sys.path.append("../modelisation/")
#from cascade import *


"""Ici la base d'entrainement est la base de M6"""

#dictionnaire des paramètres du prédicteur
d = pickle.load(open('../modelisation/my_model_parameters.pkl', 'rb'))

#fonction qui countvectorize le texte
#retourne un vecteur de la taille du vocabulaire
#à chaque indice i, on a le nombre d'occurences où le mot d'indice i apaprait
def transform_text(text, voc):
    text=np.array(text.lower().split())
    x=np.zeros(len(voc)) #initialisation avec un vecteur de 0
    words_in_voc=np.array([word for word in text if word in voc]) #liste des mots du texte qui sont dans le vocabulaire
    
    #le dictionnaire occurence compte pour chaque mot le nombre d'occurences
    unique, counts = np.unique(words_in_voc, return_counts=True)
    occurences=dict(zip(unique, counts))
    
    for word in words_in_voc:
        i=voc[word] #indice du mot dans le vocabulaire
        x[i]=occurences[word]
    return x

def logistique(x):
    return 1/(1+np.exp(-x))


#prends en paramètre la représentation countvectorizée du texte et le dico qui contient les paramètres du modèle
#prédit le label de sensibilité
def predict_label(x, dico):
    thresholds=dico["thresholds"] #les deux thresholds de classification
    beta1=dico["parameters1"][:-1] #coefs de la première régression logistique (TP VS sensibles) (on enlève le biais)
    print(len(beta1)==len(dico["word_to_id"]))
    beta2=dico["parameters2"][:-1] #coeff de la 2e régression (-10/-12 VS -16/-18) (seulement si le texte est jugé sensible)
    print(len(beta2)==len(dico["word_to_id"]))
    
    score1=np.dot(x, beta1)+dico["parameters1"][-1] #le biais est rajouté dans le calcul du score
    proba1=logistique(score1) #proba d'être sensible
    if proba1<thresholds[1]:
        label=1
    else:
        score2=np.dot(x, beta2)+dico["parameters2"][-1]
        proba2=logistique(score2) #proba d'être -16/-18
        if proba2>thresholds[2]:
            label=3 #si la proba est > au threshold on classe -16/-18
        else:
            label=2 #sinon on classe -10/-12
    return label    



app = Flask(__name__)






@app.route('/csa', methods=['POST'])
def prediction():
    data=request.get_json(force=True)
    texte=data["texte"]
    x=transform_text(texte, d["word_to_id"])
    pred=predict_label(x,d)
    trad={1:"le_texte a un contenu Tous publics", 2:"Le texte a un contenu interdit aux moins de 10 ans ou 12 ans", 3:"Le texte a un contenu interdit aux moins de 16 ou 18 ans"}
    return jsonify(label=pred, texte=trad[pred])



if __name__ == '__main__':
    app.run(debug=True, port=4000) #running on port 4000