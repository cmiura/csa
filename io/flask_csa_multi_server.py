import pickle
from flask import *
import sys 

sys.path.append("../modelisation/")
#from cascade import *


"""Ici la base d'entrainement est la base de M6"""


my_cascade_classifier = pickle.load(open('../modelisation/my_cascade_classifier.pkl', 'rb'))



app = Flask(__name__)




@app.route('/csa', methods=['POST'])
def prediction():
    data=request.get_json(force=True)
    texte=data["texte"]
    pred=my_cascade_classifier.predict_un_texte(texte)
    d={1:"le_texte a un contenu Tous publics", 2:"Le texte a un contenu interdit aux moins de 10 ans ou 12 ans", 3:"Le texte a un contenu interdit aux moins de 16 ou 18 ans"}
    return jsonify(label=pred, texte=d[pred])



if __name__ == '__main__':
    app.run(debug=True, port=4000) #running on port 4000

