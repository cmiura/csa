import unittest 
import random
import numpy as np
from sklearn import model_selection
import scipy as sp
import sys

sys.path.append("../modelisation/")
from vectorizer import *
from classifier import *


from  binary_evaluation import *

import pandas as pd
data = pd.read_csv("../io/data.csv", sep=",", encoding="utf-8", index_col='Unnamed: 0')
del data['metadescription'], data['id'], data['label'], data['TagValue']
data = data.dropna(subset=["description"])
data = data.drop_duplicates(subset=["description"])
data['description'] = data['description'].map(lambda x:x.replace("040",". ").lower())
columns = ['label', 'text', 'program_title']
data.columns = columns

X = data['text']
y = data['label'].apply(lambda x: 0 if x==1 else 1)


X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y)

cv=CountVectorizer()
X_train=cv.fit_transform(X_train)
X_valid=cv.transform(X_valid)

classifier=LogisticRegressionClassifier()
classifier=classifier.train(X_train, y_train) #on retourne le classifier entrainé # 
y_proba=classifier.predict_proba(X_valid)



class BinaryEvaluationTest(unittest.TestCase):

	def setUp(self):
		self.evaluation=Evaluation(y_valid, y_proba)
		self.confusion=self.evaluation.confusion_matrix() #seuil de classification à 0.5 par défaut au début
		#on fixe un seuil différent du seuil par défaut


	def test_confusion_matrix_good_dim(self):
		"""Teste si la matrice de confusion est bien de dimension 2,2"""
		m=self.confusion
		self.assertEqual(m.shape, (2,2))

	def test_all_in_confusion_matrix(self):
		"""Teste que tous les éléments de y_valid sont évalués par dans matrice"""
		m=self.confusion
		self.assertEqual(m.sum(), len(self.evaluation.y_true))


	def test_true_positifs_confusion_matrix(self):
		"""teste que le nombre de true positifs de la matrice de conf correspond bien au nombre de labels positifs bien prédits"""
		y_pred=np.array([1 if self.evaluation.y_proba[i] >=self.evaluation.optimal_threshold else 0 for i in range(len(self.evaluation.y_proba))]) 
		df=pd.DataFrame({"y_pred":y_pred, "y_true":self.evaluation.y_true})
		nb_true_positifs=len(df[ (df.y_pred==1) & (df.y_true==1) ])
		m=self.confusion
		self.assertEqual(nb_true_positifs, m[1,1])


	def test_true_negatifs_confusion_matrix(self):
		"""teste que le nombre de true negatifs de la matrice de conf correspond bien au nombre de labels negatifs bien prédits"""
		y_pred=np.array([1 if self.evaluation.y_proba[i] >=self.evaluation.optimal_threshold else 0 for i in range(len(self.evaluation.y_proba))]) 
		df=pd.DataFrame({"y_pred":y_pred, "y_true":self.evaluation.y_true})
		nb_true_negatifs=len(df[ (df.y_pred==0) & (df.y_true==0) ])
		m=self.confusion
		self.assertEqual(nb_true_negatifs, m[0,0])


	def test_faux_positifs_confusion_matrix(self):
		"""Teste que le nombre de faux positifs de la matrice de conf correpond bien au nombre de labels negatifs mal prédits en positifs"""
		y_pred=np.array([1 if self.evaluation.y_proba[i] >=self.evaluation.optimal_threshold else 0 for i in range(len(self.evaluation.y_proba))]) 
		df=pd.DataFrame({"y_pred":y_pred, "y_true":self.evaluation.y_true})
		nb_faux_positifs=len(df[ (df.y_pred==1) & (df.y_true==0) ])
		m=self.confusion
		self.assertEqual(nb_faux_positifs, m[1,0])


	def test_faux_negatifs_confusion_matrix(self):
		"""Teste que le nombre de faux positifs de la matrice de conf correpond bien au nombre de labels negatifs mal prédits en positifs"""
		y_pred=np.array([1 if self.evaluation.y_proba[i] >=self.evaluation.optimal_threshold else 0 for i in range(len(self.evaluation.y_proba))]) 
		df=pd.DataFrame({"y_pred":y_pred, "y_true":self.evaluation.y_true})
		nb_faux_negatifs=len(df[ (df.y_pred==0) & (df.y_true==1) ])
		m=self.confusion
		self.assertEqual(nb_faux_negatifs, m[0,1])




	def test_optimal_threshold_youden_0_1(self):
		"""teste que le threshold retourné est bien entre 0 et 1 """
		threshold=self.evaluation.find_optimal_threshold()
		self.assertTrue(0<=threshold and threshold<=1)


	def test_optimal_threshold_precision_recall_0_1(self):
		threshold=self.evaluation.find_optimal_threshold(metric='precision_recall')
		self.assertTrue(0<=threshold and threshold<=1)



	def test_roc_auc_0_1(self):
		"""Teste si l'AUC est bien entre 0 et 1"""
		auc=self.evalaluation.roc_auc_score()
		self.assertTrue(0<= auc and auc <=1)








if __name__=='__main__':
	unittest.main()


