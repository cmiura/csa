import unittest 
import random
import numpy as np
from sklearn import model_selection
import scipy as sp
import sys

sys.path.append("../modelisation/")
from vectorizer import *
from classifier import *




import pandas as pd
data = pd.read_csv("../io/data.csv", sep=",", encoding="utf-8", index_col='Unnamed: 0')
del data['metadescription'], data['id'], data['label'], data['TagValue']
data = data.dropna(subset=["description"])
data = data.drop_duplicates(subset=["description"])
data['description'] = data['description'].map(lambda x:x.replace("040",". ").lower())
columns = ['label', 'text', 'program_title']
data.columns = columns

X = data['text']
y = data['label'].apply(lambda x: 0 if x==1 else 1)


X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y)




class DataTest(unittest.TestCase):
    
	"""Test case utilisé pour tester que les données du vectorizer sont au bon format"""
    
	def setUp(self):
		self.X = data["text"]
                      
	def test_X_not_null(self):
		"""Test qu'on a pas de NaN dans X"""
		res=self.X.isnull().any()
		self.assertFalse(res)
        
	def test_X_str(self):
		"""test qu'on a que des chaînes de charactères dans X"""
		test=np.array([True if type(text)==str else False for text in self.X ]) #array de la taille de X. =True si on a bien une chaine de charactères
		self.assertFalse(False in test) #test si on a des False dans test
        

	def test_1_classe_minoritaire(self):
		
		"""On vérifie que la classe 1 est la classe minoritaire"""
		taille_classe_positive=len(y[y==1])
		taille_classe_negative=len(y[y==0])
		self.assertTrue(taille_classe_positive<taille_classe_negative)	
		


class CountVectorizerTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.cv=CountVectorizer()
		#self.vocabulary = CountVectorizer().sparse_vectorizer.get_feature_names()
		self.results_train = self.cv.fit_transform(self.X_train)
		self.results_valid=self.cv.transform(self.X_valid)
		self.vocabulary=self.cv.sparse_vectorizer.get_feature_names()


	def test_train_is_sparse(self):

		"""Test si la base d'entrainement est bien sparse """
		#self.assertIsInstance(results, sp.sparse.csr.csr_matrix)
		self.assertTrue(isinstance(self.results_train, sp.sparse.csr.csr_matrix))

	def test_valid_is_sparse(self):
		""" Teste si la base de validation est bien sparse"""
		self.assertTrue(isinstance(self.results_valid, sp.sparse.csr.csr_matrix))



	def test_train_int(self):
		"""teste si on a bien des entiers dans la matrice sparse"""
		self.assertTrue(self.results_train.dtype=="int64")


	def test_valid_int(self):
		self.assertTrue(self.results_valid.dtype=="int64")


	def test_train_good_dim(self):
		"""teste que la base de train est bien une matrice (nb_descriptions dans train x taille voc)"""
		nb_descriptions=len(self.X_train)
		#vocabulary
		taille_voc=len(self.vocabulary)
		self.assertEqual(self.results_train.shape, (nb_descriptions, taille_voc))


	def test_valid_good_dim(self):
		nb_descriptions=len(self.X_valid)
		#vocabulary=self.cv.sparse_vectorizer.get_feature_names()
		taille_voc=len(self.vocabulary)
		self.assertEqual(self.results_valid.shape, (nb_descriptions, taille_voc))






class TfidfTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.tfidf=TfidfVectorizer()
		#self.vocabulary = CountVectorizer().sparse_vectorizer.get_feature_names()
		self.results_train = self.tfidf.fit_transform(self.X_train)
		self.results_valid=self.tfidf.transform(self.X_valid)
		self.vocabulary=self.tfidf.sparse_vectorizer.get_feature_names()


	def test_train_is_sparse(self):
		#self.assertIsInstance(results, sp.sparse.csr.csr_matrix)
		self.assertTrue(isinstance(self.results_train, sp.sparse.csr.csr_matrix))

	def test_valid_is_sparse(self):
		self.assertTrue(isinstance(self.results_valid, sp.sparse.csr.csr_matrix))


	def test_train_float(self):
		"""teste si on a bien des float dans la matrice sparse"""
		self.assertTrue(self.results_train.dtype=="float64")

	def test_valid_float(self):
		self.assertTrue(self.results_valid.dtype=="float64")



	def test_train_good_dim(self):
		nb_descriptions=len(self.X_train)
		#vocabulary
		taille_voc=len(self.vocabulary)
		self.assertEqual(self.results_train.shape, (nb_descriptions, taille_voc))


	def test_valid_good_dim(self):
		nb_descriptions=len(self.X_valid)
		#vocabulary=self.hashing.sparse_vectorizer.get_feature_names()
		taille_voc=len(self.vocabulary)
		self.assertEqual(self.results_valid.shape, (nb_descriptions, taille_voc))




class HashingVectorizerTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.hashing=HashingVectorizer()
		#self.vocabulary = CountVectorizer().sparse_vectorizer.get_feature_names()
		self.results_train = self.hashing.fit_transform(self.X_train)
		self.results_valid=self.hashing.transform(self.X_valid)
		


	def test_train_is_sparse(self):
		#self.assertIsInstance(results, sp.sparse.csr.csr_matrix)
		self.assertTrue(isinstance(self.results_train, sp.sparse.csr.csr_matrix))

	def test_valid_is_sparse(self):
		self.assertTrue(isinstance(self.results_valid, sp.sparse.csr.csr_matrix))



	def test_train_float(self):
		"""teste si on a bien des float dans la matrice sparse"""
		self.assertTrue(self.results_train.dtype=="float64")

	def test_train_float(self):
		self.assertTrue(self.results_valid.dtype=="float64")





class Word2VecVectorizerTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.word2vec=Word2VecVectorizer()
		self.results_train = self.word2vec.fit_transform(self.X_train)
		self.results_valid=self.word2vec.transform(self.X_valid)
		self.index=list(self.word2vec.tokenizer.word_index.values()) #Pour le word2vec avec vecteur non préentrainé
		





	def test_train_good_dim(self):

		""" Teste si la matrice train est de la bonne dimension soit nombre de description x nombre de mots de la description la plus longue"""
		nb_descriptions=len(self.X_train)
		#taille de la description la plus longue
		data_preprocessed=self.X_train.apply(lambda x:preprocess(x))
		ind=data_preprocessed.map(lambda x:len(x)).argmax()
		taille_max=len(data_preprocessed[ind].split())
		self.assertEqual(self.results_train.shape, (nb_descriptions, taille_max))


	def test_valid_good_dim(self):
		""" Teste si la matrice valid est de la bonne dimension soit nombre de description x nombre de mots de la description la plus longue"""
		nb_descriptions=len(self.X_valid)
		#taille de la description la plus longue mais de la base train
		data_preprocessed=self.X_train.apply(lambda x:preprocess(x))
		ind=data_preprocessed.map(lambda x:len(x)).argmax()
		taille_max=len(data_preprocessed[ind].split())
		self.assertEqual(self.results_valid.shape, (nb_descriptions, taille_max))


	def test_int_train(self):

		"""Teste si la matrice train est bien une matrice d'entiers de type "int32" """
		self.assertTrue(self.results_train.dtype=="int32")


	def test_int_valid(self):
		"""Teste si la matrice valid est bien une matrice d'entiers de type "int32" """
		self.assertTrue(self.results_valid.dtype=="int32")


	def test_valid_in_index(self):
		"""Teste que tous les éléments de la matrice valid sont dans l'index"""
		index_valid=np.unique(self.results_valid).tolist()[1:] #on enlève le premier élément qui est 0 
		for x in index_valid:
			self.assertIn(x, self.index)







class Doc2VecVectorizerTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.doc2vec=Doc2VecVectorizer()
		self.results_train = self.doc2vec.fit_transform(self.X_train)
		self.results_valid=self.doc2vec.transform(self.X_valid)
		self.vector_size=self.doc2vec.vector_size
		





	def test_train_good_dim(self):

		""" Teste si la matrice train est de la bonne dimension soit nombre de descriptions de X_train x la taille du vecteur """
		nb_descriptions=len(self.X_train)
		self.assertEqual(self.results_train.shape, (nb_descriptions, self.vector_size))


	def test_valid_good_dim(self):
		""" Teste si la matrice valid est de la bonne dimension soit nombre de descriptions de X_valid x la taille du vecteur"""
		nb_descriptions=len(self.X_valid)
		self.assertEqual(self.results_valid.shape, (nb_descriptions, self.vector_size))


	def test_float_train(self):
		self.assertTrue(self.results_train.dtype=="float64")


	def test_float_valid(self):
		self.assertTrue(self.results_valid.dtype=="float64")




class SVDVectorizerTest(unittest.TestCase):

	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.SVD=SVDVectorizer()
		self.sparse_vectorizer=CountVectorizer()
		self.X_train_sparse=self.sparse_vectorizer.fit_transform(X_train)
		self.X_valid_sparse=self.sparse_vectorizer.transform(X_valid)
		self.results_train = self.SVD.fit_transform(self.X_train_sparse)
		self.results_valid=self.SVD.transform(self.X_valid_sparse)
		self.n_components=self.SVD.n_components
		





	def test_train_good_dim(self):

		""" Teste si la matrice train est de la bonne dimension soit nombre de descriptions de X_train x la taille du vecteur """
		nb_descriptions=len(self.X_train)
		self.assertEqual(self.results_train.shape, (nb_descriptions, self.n_components))


	def test_valid_good_dim(self):
		""" Teste si la matrice valid est de la bonne dimension soit nombre de descriptions de X_valid x la taille du vecteur"""
		nb_descriptions=len(self.X_valid)
		self.assertEqual(self.results_valid.shape, (nb_descriptions, self.n_components))


	def test_float_train(self):
		self.assertTrue(self.results_train.dtype=="float64")


	def test_float_valid(self):
		self.assertTrue(self.results_valid.dtype=="float64")






	
        





if __name__=='__main__':
	unittest.main()