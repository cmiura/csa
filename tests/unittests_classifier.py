import unittest 
import random
import numpy as np
from sklearn import model_selection
import scipy as sp

sys.path.append("../modelisation/")
from vectorizer import *
from classifier import *
#import binary_evaluation

import pandas as pd
data = pd.read_csv("../io/data.csv", sep=",", encoding="utf-8", index_col='Unnamed: 0')
del data['metadescription'], data['id'], data['label'], data['TagValue']
data = data.dropna(subset=["description"])
data = data.drop_duplicates(subset=["description"])
data['description'] = data['description'].map(lambda x:x.replace("040",". ").lower())
columns = ['label', 'text', 'program_title']
data.columns = columns

X = data['text']
y = data['label'].apply(lambda x: 0 if x==1 else 1)


X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y)

cv=CountVectorizer()
X_train=cv.fit_transform(X_train)
X_valid=cv.transform(X_valid)


class DataTest(unittest.TestCase):
	""" teste que les donnes sont au bon format"""

	def setUp(self):
		self.y=y
		self.X_train=X_train
		self.X_valid=X_valid
		self.y_train=y_train
		self.y_valid=y_valid


	def test_0_1(self):
		""" teste que les labels sont bien des 0 ou des 1"""
		for elt in self.y:
			self.assertIn(elt, np.array([0,1]))

	def test_good_dim_train(self):
		"""teste que le nombre de lignes de X_train = longueur de y_train"""
		self.assertEqual(self.X_train.shape[0], len(self.y_train))

	def test_good_dim_valid(self):
		"""teste que le nombre de lignes de X_valid = longueur de y_valid"""
		self.assertEqual(self.X_valid.shape[0], len(self.y_valid))


	






class LogisticRegressionClassifierTest(unittest.TestCase):


	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.y_train=y_train
		self.y_valid=y_valid
		self.classifier=LogisticRegressionClassifier()
		self.classifier=self.classifier.train(self.X_train, self.y_train) #on retourne le classifier entrainé #
		self.threshold=0.8 #seuil de classification choisi pour les probas
		self.y_proba=self.classifier.predict_proba(self.X_valid)
		self.y_pred=self.classifier.predict(self.X_valid, self.threshold)


	def test_pred_proba_is_proba(self):
		""" On teste que les probas prédites sont bien comprise entre 0 et 1 """
		for proba in self.y_proba:
			self.assertTrue(0<=proba and proba <=1)

	def test_pred_0_1(self):
		""" test que les predictions sont bien des 0 ou des 1"""
		for y in self.y_pred:
			self.assertIn(y, np.array([0,1]))


	def test_pred_proba_good_dim(self):
		"""teste que la longueur des probas prédites est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_proba), len(self.y_valid))


	def test_pred_good_dim(self):
		"""Teste que la longueur des prédictions est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_pred), len(self.y_valid))



	def test_above_threshold(self):
		"""teste si les predictions 1 sont bien associées aux probas supérieures au threshold"""
		df=pd.DataFrame({"predictions":self.y_pred, "probas_positives":self.y_proba})
		self.assertNotIn(0, df.predictions[df.probas_positives>self.threshold])





class RandomForestClassifierTest(unittest.TestCase):


	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.y_train=y_train
		self.y_valid=y_valid
		self.classifier=RandomForestClassifier()
		self.classifier=self.classifier.train(self.X_train, self.y_train) #on retourne le classifier entrainé #
		self.threshold=0.8 #seuil de classification choisi pour les probas
		self.y_proba=self.classifier.predict_proba(self.X_valid)
		self.y_pred=self.classifier.predict(self.X_valid, self.threshold)


	def test_pred_proba_is_proba(self):
		""" On teste que les probas prédites sont bien comprise entre 0 et 1 """
		for proba in self.y_proba:
			self.assertTrue(0<=proba and proba <=1)

	def test_pred_0_1(self):
		""" test que les predictions sont bien des 0 ou des 1"""
		for y in self.y_pred:
			self.assertIn(y, np.array([0,1]))


	def test_pred_proba_good_dim(self):
		"""teste que la longueur des probas prédites est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_proba), len(self.y_valid))


	def test_pred_good_dim(self):
		"""Teste que la longueur des prédictions est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_pred), len(self.y_valid))



	def test_above_threshold(self):
		"""teste si les predictions 1 sont bien associées aux probas supérieures au threshold"""
		df=pd.DataFrame({"predictions":self.y_pred, "probas_positives":self.y_proba})
		self.assertNotIn(0, df.predictions[df.probas_positives>self.threshold])






class GradientBoostingClassifierTest(unittest.TestCase):


	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.y_train=y_train
		self.y_valid=y_valid
		self.classifier=GradientBoostingClassifier()
		self.classifier=self.classifier.train(self.X_train, self.y_train) #on retourne le classifier entrainé #
		self.threshold=0.8 #seuil de classification choisi pour les probas
		self.y_proba=self.classifier.predict_proba(self.X_valid)
		self.y_pred=self.classifier.predict(self.X_valid, self.threshold)


	def test_pred_proba_is_proba(self):
		""" On teste que les probas prédites sont bien comprise entre 0 et 1 """
		for proba in self.y_proba:
			self.assertTrue(0<=proba and proba <=1)

	def test_pred_0_1(self):
		""" test que les predictions sont bien des 0 ou des 1"""
		for y in self.y_pred:
			self.assertIn(y, np.array([0,1]))


	def test_pred_proba_good_dim(self):
		"""teste que la longueur des probas prédites est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_proba), len(self.y_valid))


	def test_pred_good_dim(self):
		"""Teste que la longueur des prédictions est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_pred), len(self.y_valid))



	def test_above_threshold(self):
		"""teste si les predictions 1 sont bien associées aux probas supérieures au threshold"""
		df=pd.DataFrame({"predictions":self.y_pred, "probas_positives":self.y_proba})
		self.assertNotIn(0, df.predictions[df.probas_positives>self.threshold])



""""Tests pour les deep_classifiers"""

X_train, X_valid, y_train, y_valid = model_selection.train_test_split(X, y)

cv=Word2VecVectorizer()
X_train=cv.fit_transform(X_train)
X_valid=cv.transform(X_valid)


class MLPClassifierTest(unittest.TestCase):





	def setUp(self):
		self.X_train=X_train
		self.X_valid=X_valid
		self.y_train=y_train
		self.y_valid=y_valid
		self.classifier=MLPClassifier(input_dim=np.max(X_train)+1, output_dim=50, input_length=X_train.shape[1])
		self.classifier=self.classifier.train(self.X_train, self.y_train) #on retourne le classifier entrainé #
		self.threshold=0.8 #seuil de classification choisi pour les probas
		self.y_proba=self.classifier.predict_proba(self.X_valid)
		self.y_pred=self.classifier.predict(self.X_valid, self.threshold)


	def test_pred_proba_is_proba(self):
		""" On teste que les probas prédites sont bien comprise entre 0 et 1 """
		for proba in self.y_proba:
			self.assertTrue(0<=proba and proba <=1)

	def test_pred_0_1(self):
		""" test que les predictions sont bien des 0 ou des 1"""
		for y in self.y_pred:
			self.assertIn(y, np.array([0,1]))


	def test_pred_proba_good_dim(self):
		"""teste que la longueur des probas prédites est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_proba), len(self.y_valid))


	def test_pred_good_dim(self):
		"""Teste que la longueur des prédictions est égale à la longueur de y_valid"""
		self.assertEqual(len(self.y_pred), len(self.y_valid))



	def test_above_threshold(self):
		"""teste si les predictions 1 sont bien associées aux probas supérieures au threshold"""
		df=pd.DataFrame({"predictions":self.y_pred, "probas_positives":self.y_proba})
		self.assertNotIn(0, df.predictions[df.probas_positives>self.threshold])




	


















if __name__=='__main__':
	unittest.main()
